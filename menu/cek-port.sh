#!/bin/bash

xrayport="$(netstat -ntlp | grep -i xray | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
nginxport="$(netstat -ntlp | grep -i nginx | grep -i 0.0.0.0 | awk '{print $4}' | cut -d: -f2)"
clear
echo ""
echo ""
echo -e "\e[0;37m                    Port Yang Sedang Aktif      "
echo -e "\e[0;37m                    Service Port                "
echo -e "\e[0m                                                   "
echo -e "\e[0;37m                    Port Nginx      :  "$nginxport
echo -e "\e[0;37m                    Port Xray       :  "$xrayport
echo -e "\e[0m                                                   "
echo -e "\e[0;37m                    * Info                      "
echo -e "\e[0;37m                    * Jika port nya tidak muncul"
echo -e "\e[0;37m                    * berarti port nya mati/OFF "
echo ""
read -sp " Press ENTER to go back"
echo ""
